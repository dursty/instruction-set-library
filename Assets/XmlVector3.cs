using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class XmlVector3 {
    [XmlAttribute("x")]
    public int x;
    [XmlAttribute("y")]
    public int y;
    [XmlAttribute("z")]
    public int z;

    public Vector3 getVector3(){
        return new Vector3(x,y,z);
    }
}
