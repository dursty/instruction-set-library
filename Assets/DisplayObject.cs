using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class DisplayObject {
    public enum ModelType {
        Arrow = 0
    }

    [XmlAttribute("id")]
    public int id;

    [XmlElement(Type = typeof(XmlVector3))]
	public XmlVector3 Translation;

    [XmlAttribute("modelType")]
    public ModelType modelType;

    [XmlAttribute("orientation")]
    public string orientation;

    public Vector3 getTranslateCoord(){
        return Translation.getVector3();
    }

    public Vector3 getRotateCoord(){
        switch(orientation){
                case "right":
                    return Vector3.right;
                case "left":
                    return Vector3.left;
                case "up":
                    return Vector3.up;
                case "down":
                    return Vector3.down;
                case "back":
                    return Vector3.back;
                case "forward":
                    return Vector3.forward;
                default:
                    return Vector3.zero;
        }
    }

    public ModelType getModel(){
        return modelType;
    }
}
