﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleInstructionReceiver : InstructionReceiverInterface {

    private TextMesh txt;

    void Start(){
        txt = GetComponent<TextMesh>();
    }

	public override void onNewInstruction(InstructionStep instruction){
		txt.text = instruction.getText()[ID];
    }
}
