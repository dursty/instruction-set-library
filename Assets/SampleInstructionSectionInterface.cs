﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleInstructionSectionInterface : InstructionSectionInterface {

	private TextMesh txt;

	void Start(){
		txt = GetComponent<TextMesh>();
	}

	public override void onNewSection(InstructionSection section){
		txt.text = "Section Name: "+ section.getName();
	}
}
